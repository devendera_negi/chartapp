import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {
  DataForLineChart
} from '../parser';
import {
    ENVIRONMENT
  } from '../../constant';

@Injectable()
export class DataService {
  public dataString: any;

  constructor(
    private http: Http
  ) { }

  public setRequestObject(apiUrl, payload) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers });
    let requestObj = this.http.post(apiUrl, payload, options);

    if ( ENVIRONMENT === 'local' ) {
      requestObj = this.http.get(apiUrl, options);
    }
    return requestObj;
  }

  public getData(apiurl, payload) {
    let dataArray = [];
    return this.setRequestObject(apiurl, payload)
      .map(response => {
        return response.json();
        // return DataForLineChart(response.json(), 'test');
      });
  }
}