import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment'
import { DataService } from './service';
import { close } from 'fs';

@Component({
  selector: 'app-root',
  providers: [DataService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'test title';
  public chartData;
  public name = "chart";
  public nowHour;
  public barChartData;
  public yLabelText = 'ylabel';
  
  constructor(
    public dataService: DataService
) { }
  public ngOnInit() {
    const api = 'http://localhost:8087/chartMockData';
    const payload = {};    
    this.dataService
      .getData(api, payload)
      .subscribe((data: any) => {
        const myData = data.result;
        const date = new Date();
        this.nowHour = date.getHours();
        const temp = _.zipWith(myData.data, myData.x, (a, b) => {
          return {data: a, x: b};
        });
        console.log(temp);
        this.chartData = temp;
      });
      const barchartApi = 'http://localhost:8087/barChartData';
      this.dataService
        .getData(barchartApi, payload)
        .subscribe((data: any) => {
          const result = data.result;
          const values = [];
          const tmpObj = _.zipObject(result.month, result.data);
          console.log('tempObj:-', tmpObj);
          _.forEach(tmpObj, (o, key) => {
            values.push([key, parseInt(o, 10)]);
          });
          console.log('values:-', values);
          this.barChartData = values;
        });
  }
}
