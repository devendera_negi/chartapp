import {
    Component,
    OnChanges,
    AfterViewInit,
    Input,
    ElementRef,
    ViewChild,
    ViewEncapsulation
  } from '@angular/core';
  import * as d3 from 'd3';
  import * as moment from 'moment';
  
  @Component({
    selector: 'line-chart',
    templateUrl: './linechart.component.html',
    styleUrls: ['./linechart.component.css'],
    encapsulation: ViewEncapsulation.None
  })

  export class LineChartComponent {
    @ViewChild('chart') private chartContainer: ElementRef;
    @Input() private now: number;
    @Input() private data: Array<{data, x, y}>;
    @Input() private itemName: string;
    private margin: any = { top: 20, bottom: 20, left: 20, right: 20};
    private chart: any;
    private width: number;
    private height: number;
    private xScale: any;
    private yScale: any;
    private colors: any;
    private xAxis: any;
    private yAxis: any;

    public ngOnInit() {
        // this.createChart();
        if (this.data) {
        this.createChart();
        }
    }
    public ngOnChanges() {
        if (this.data && !this.isEmpty(this.data)) {
            this.createChart();
        }
    }
    public make_y_gridlines(yScale) {
        return d3.axisLeft(yScale)
            .ticks(9);
    }
    public isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

    public createChart() {
        let element = this.chartContainer.nativeElement;
        this.width = element.offsetWidth - this.margin.left - this.margin.right;
        this.height = element.offsetHeight - this.margin.top - this.margin.bottom;
    
        let svg = d3.select(element)
                    .append('svg')
                    .attr('width', element.offsetWidth)
                    .attr('height', element.offsetHeight)
                    .attr('class', 'svg-chart');

        this.chart = svg.append('g')
                    .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);
        /**
         * define X & Y domains
         */
        
        const yDomain = [0, d3.max(this.data, (d) => d.data)];        
        const xDomain = this.data.map(d => d.x);

        this.xScale = d3.scaleBand()
            .domain(xDomain)
            .rangeRound([0, this.width]);
        this.yScale = d3.scaleLinear()
            .domain(yDomain)
            .range([this.height, 0]);
        this.chart.append('g')
            .attr('class', 'grid-y')
            // .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
            .call(this.make_y_gridlines(this.yScale)
            .tickSize(-this.width));

        /**
         * Draw line graph path
         */
        const line = d3.line()
            .curve(d3.curveCatmullRomOpen)
            .x( (d: any) => {
                console.log('xline:-', d.x, this.xScale(d.x));
                return this.xScale(d.x) 
            })
            .y( (d: any) => {
                console.log('yline:-', d.data, this.xScale(d.data));
                return this.yScale(d.data)
            });

        const div = d3.select(element)
            .append('div')
            .attr('class', 'chartTooltip')
            .style('opacity', 0);

        this.chart
            .append('path')
            .attr('stroke', '#fff')
            .datum(this.data)
            .attr('class', 'line')
            .attr('d', line);
        /**
         * Add dot on chart
         */
        this.chart
            .append('g')
            .attr('class', 'axis axisX')
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(d3.axisBottom(this.xScale));
        this.chart
            .selectAll('dot')
            .data(this.data)
            .enter()
            .append('circle')
            .attr('class', 'dot-circle')
            .attr('r', 4)
            .attr('cx', (d) => {
                  return this.xScale(d.x);
              })
            .attr('cy', (d) => { return this.yScale(d.data); })
            .on('mouseover', (d) => {
                div.transition()
                    .duration(200)
                    .style('opacity', .9)
                    .style('visibility', 'visible');
                div.html('time:-' + d.x + '<br/>value:-'  + d.data)
                    .style('left', (d3.event.layerX  - 40) + 'px')
                    .style('top', (d3.event.layerY + 18) + 'px');
            })
            .on('mouseout', (d) => {
                div.transition()
                .duration(500)
                .style('opacity', 0);
            });

         /**
         * draw now line
         */
        this.chart
            .append("line")
            .attr("class", "zero")
            .attr("x1", this.xScale(this.now))
            .attr("y1", 0)
            .attr("x2", this.xScale(this.now))
            .attr("y2", this.height)
            .attr("transform", "translate(30,0)");
        this.chart
            .append('rect')
            .attr("x", this.xScale(this.now) + 5)
            .attr("y", 0)
            .attr("width", 50)
            .attr("height", 30);
        this.chart
            .append("text") 
            .attr("class", "now-text")
            .attr("x", this.xScale(this.now) + 30 )
            .attr("y", 20)
            .attr("fill", "black")
            .attr("text-anchor", "middle")
            .text("NOW");
    }
    
  }